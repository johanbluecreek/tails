# SOME DESCRIPTIVE TITLE
# Copyright (C) YEAR Free Software Foundation, Inc.
# This file is distributed under the same license as the PACKAGE package.
# FIRST AUTHOR <EMAIL@ADDRESS>, YEAR.
#
msgid ""
msgstr ""
"Project-Id-Version: PACKAGE VERSION\n"
"POT-Creation-Date: 2019-08-29 08:06+0000\n"
"PO-Revision-Date: 2019-09-25 13:31+0000\n"
"Last-Translator: xin <xin@riseup.net>\n"
"Language-Team: LANGUAGE <LL@li.org>\n"
"Language: fr\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=n > 1;\n"
"X-Generator: Weblate 2.20\n"

#. type: Content of: <p>
msgid ""
"Many publicly accessible Internet connections (usually available through a "
"wireless network connection) require users to first log in to a <em>captive "
"portal</em> in order to access the Internet."
msgstr ""
"De nombreuses connexions à Internet accessibles publiquement (généralement "
"disponibles via une connexion réseau sans fil) nécessitent de s'identifier "
"sur un <em>portail captif</em> pour pouvoir accéder à Internet."

#. type: Content of: <p>
msgid ""
"A captive portal is a web page that is displayed to the user before the user "
"can access the Internet. Captive portals usually require the user to log in "
"to the network or enter information such as an email address. Captive "
"portals are commonly encountered at Internet cafés, libraries, airports, "
"hotels, and universities."
msgstr ""
"Un portail captif est une page web qui est affichée avant d'accéder à "
"Internet. Les portails captifs nécessitent généralement de s'identifier ou "
"d'entrer des informations comme une adresse email. Les portails captifs sont "
"souvent rencontrés dans des cafés Internet, des bibliothèques, des "
"aéroports, des hôtels et des universités."

#. type: Content of: <p>
msgid ""
"This is an example of a captive portal (by <a "
"href=\"https://commons.wikimedia.org/wiki/File:Captive_Portal.png\">AlexEng</a>):"
msgstr ""
"Voici un exemple de portail captif (par <a href=\"https://commons.wikimedia."
"org/wiki/File:Captive_Portal.png\">AlexEng</a>) :"

#. type: Content of: outside any tag (error?)
msgid ""
"[[!img captive-portal.png link=\"no\" alt=\"Welcome! Please enter your "
"credentials to connect.\"]]"
msgstr ""
"[[!img captive-portal.png link=\"no\" alt=\"Welcome! Please enter your "
"credentials to connect.\"]]"
