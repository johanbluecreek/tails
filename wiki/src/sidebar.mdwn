<div class="download button">
  [[<span class="install">Install</span>
    <span class="tails">Tails [[!inline pages="inc/stable_amd64_version" raw="yes" sort="age"]]</span>
    <span class="date">[[!inline pages="inc/stable_amd64_date" raw="yes" sort="age"]]</span>|install]]
</div>

<div id="tor_check" class="button">
<a href="https://check.torproject.org/">
[[!img "lib/onion.png" link="no"]]
<!-- Note for translators: You can use <span class="twolines"> if your
translation of the label below is long and gets split into two lines. -->
<span>Tor check</span>
</a>
</div>

<div class="links">
  <ul>
    <li>[[About|about]]</li>
    <li>[[Getting started…|getting started]]</li>
    <li>[[Documentation|doc]]</li>
    <li>[[Help &amp; Support|support]]</li>
    <li>[[Contribute]]</li>
    <li>[[News|news]]</li>
    <li>[[Jobs|jobs]]</li>
  </ul>
</div>

<div class="donate button">
    <a href="https://tails.boum.org/donate/?r=sidebar">Donate</a>
</div>
